namespace Interp.Lib;

public class SyntaxAnalyzer
{
    private IEnumerable<Lexem> _source;
    private int _position = -1;

    private SymbolsTable _symbolTable;

    private Lexem _current => _source.ElementAt(_position);

    public Queue<string> Errors {get;} = new ();

    public SyntaxAnalyzer(IEnumerable<Lexem> source, SymbolsTable symbolTable)
    {
        _source = source;
        _symbolTable = symbolTable;
    }


    // <op_assign> :== LexemType.Id & LexemType.Assign & <expression>;
    // <expression> :== LexemType.Id | LexemType.Const | <bin_expression>;
    // <bin_expression> :== <expression> & <bin_op> & <expression>;
    public void Analyze()
    {
         while(MoveNext())
         {
             switch (_current.Type)
             {
                 case LexemType.Id: 
                     Id();
                     break;
                default: 
                     Errors.Enqueue(ErrorMessage(_current, $"Unknown lexem '{_current}'"));
                     break;
             }
         }
    }

    private void Id()
    {
        if(!MoveNext()){}
        switch(_current.Type)
        {
            case LexemType.Assign: 
                Assign();
                break;
            case LexemType.SemiColon: break;
            default: 
                Errors.Enqueue(ErrorMessage(_current, "There is no semi colon or assign op after id"));
                break;

        }
    }

    private SymbolDescription.Const? Const()
    {
        SymbolDescription.Const description = null!;
        description = (SymbolDescription.Const)_symbolTable[_current.Pointer]!.Value.Description;

        if(!MoveNext()){}
        switch(_current.Type)
        {
            case LexemType.Add or LexemType.Mul or LexemType.Sub or LexemType.Div: 
                BinExpression();
                return description;

            case LexemType.SemiColon: 
                return description;

            default: 
                Errors.Enqueue(ErrorMessage(_current, "There is no semi colon or assign op after id"));
                return description;
        }
    }

    private void Assign()
    {
        if (!MoveNext()){}

        switch(_current.Type)
        {
            case LexemType.Id: Id(); break;
            case LexemType.Const: 
                var symbolDescription = Const(); 
                if(TryPeekAt(_position - 3, out var id))
                {
                    _symbolTable[id!.Value.Pointer] = new(id.Value.Value, symbolDescription!.ToVar());
                }
                break;
        }
    }

    private void BinExpression()
    {
        if (!MoveNext()){}

        switch(_current.Type)
        {
            case LexemType.Const: Const(); break;
            case LexemType.Id: Id(); break;
            default: 
                Errors.Enqueue(ErrorMessage(_current, "There should be expression or const value or id"));
                break;
        }
    }

    private bool TryPeekAtNext(out Lexem? lexem) => TryPeekAt(_position + 1, out lexem);
    private bool TryPeekAtPrev(out Lexem? lexem) => TryPeekAt(_position - 1, out lexem);

    private bool TryPeekAt(int i, out Lexem? lexem)
    {
        lexem = null;
        if (i >= _source.Count()) return false;
        
        lexem = _source.ElementAt(i);
        return true;
    }

    private bool MoveNext()
    {
        if (_position >= _source.Count() - 1) return false;
        _position++;
        return true;
    }

    private string ErrorMessage(Lexem lexem, string msg) => $"[ERROR:{lexem.Line}:{lexem.Column}]:  {msg}";
}
