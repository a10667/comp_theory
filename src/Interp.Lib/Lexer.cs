using System.Net.Security;
using System.Text;

namespace Interp.Lib;

public enum LexemType 
{
    Keyword, Id, OpenParent, ClosingParent, Const,
    SemiColon, LessThan, GreaterThan, Equals, Assign,
    Add, Mul, Sub, Div, Inc, Dec, True, False
    
}

public record struct Lexem(LexemType Type, string Value, int Pointer)
{
    public int Line { get; set; } = -1;
    public int Column { get; set; } = -1;
    
    public static Lexem OpenParant => new Lexem(LexemType.OpenParent, "(", -1);
    public static Lexem CloseingParant => new Lexem(LexemType.ClosingParent, ")", -1);
    public static Lexem SemiColon => new Lexem(LexemType.SemiColon, ";", -1);
    public static Lexem LessThan => new Lexem(LexemType.LessThan, "<", -1);
    public static Lexem GreaterThan => new Lexem(LexemType.GreaterThan, ">", -1);
    public static Lexem Equalz => new Lexem(LexemType.Equals, "=", -1);
    public static Lexem Assign => new Lexem(LexemType.Assign, ":=", -1);
    public static Lexem Add => new Lexem(LexemType.Add, "+", -1);
    public static Lexem Sub => new Lexem(LexemType.Sub, "-", -1);
    public static Lexem Mul => new Lexem(LexemType.Mul, "*", -1);
    public static Lexem Div => new Lexem(LexemType.Div, "/", -1);
    public static Lexem Inc => new Lexem(LexemType.Inc, "++", -1);
    public static Lexem Dec => new Lexem(LexemType.Dec, "--", -1);

    public static Lexem Const(string value, int pointer)
    {
        return new Lexem(LexemType.Const, value, pointer);
    }

    public static Lexem Id(string value, int pointer)
    {
        return new Lexem(LexemType.Id, value, pointer);
    }

    public static Lexem Keyword(string value)
    {
        return new Lexem(LexemType.Keyword, value, -1);
    }
}

public static class LexemExtensions
{
    public static Lexem At(this Lexem source, int line, int column)
    {
        source.Line = line;
        source.Column = column;
        return source;
    }
}


public class Lexer(string source, SymbolsTable symbolsTable)
{
    private int _position = -1;
    private static readonly List<string> Keywords =
    [
        "for", "do", "if", "then", "else", "true", "false", "or",
        "xor", "and", "not"
    ];

    private int _line = 0, _column = 0;
    public Queue<string> Errors { get; } = new();

    public List<Lexem> Lex()
    {
        var res = new List<Lexem>();
        while(MoveNext())
        {
            var current = source[_position];
            if(char.IsLetter(current))
            {
                var word = ConsumeWhile(char.IsLetter);
                if(!Keywords.Contains(word)) 
                {
                    if (string.IsNullOrEmpty(word) && string.IsNullOrWhiteSpace(word)) continue;
                    var pointer = symbolsTable.Add(word, SymbolDescription.UnknownVar);
                    res.Add(Lexem.Id(word, pointer).At(_line, _column));
                } 
                else 
                {
                    if (word is "true" or "false")
                    {
                        var pointer = symbolsTable.Add(word, SymbolDescription.BoolConst);
                        res.Add(Lexem.Const(word, pointer).At(_line, _column));
                    }
                    else 
                        res.Add(Lexem.Keyword(word).At(_line, _column));
                }
                _position--;
            }
            else if (char.IsDigit(current))
            {
                var value = ConsumeWhile(c => char.IsDigit(c) || c == '.');
                var pointer = value switch
                {
                    _ when int.TryParse(value, out _) => symbolsTable.Add(value, SymbolDescription.IntConst),
                    _ when float.TryParse(value, out _) => symbolsTable.Add(value, SymbolDescription.FloatConst),
                    _ => -1
                };
                _position--;
                res.Add(Lexem.Const(value, pointer).At(_line, _column));
                _column += value.Length - 1;
            }
            else switch (current)
            {
                case ':' when !MoveNext():
                    Errors.Enqueue(ErrorMessage("Did you mean to type ':=' ?"));
                    break;
                case ':':
                {
                    var next = source[_position];
                    if (next != '=')
                    {
                        Errors.Enqueue(ErrorMessage("Did you mean to type ':=' ?"));
                    }
                    res.Add(Lexem.Assign.At(_line, _column));
                    break;
                }
                case '/' when !MoveNext():
                    Errors.Enqueue(ErrorMessage("Did you mean to type '//' ?"));
                    break;
                case '/':
                {
                    var next = source[_position];
                    switch (next)
                    {
                        case '/':
                            ConsumeWhile(c => c != '\n' && c != '\r');
                            break;
                        case '*':
                        {
                            var text = ConsumeWhile(c => c != '/');
                            if (!MoveNext())
                            {
                                Errors.Enqueue(ErrorMessage("Multiline comment wasn't properly closed"));
                            }

                            var c = source[_position - 2];
                            if(c != '*')
                            {
                                Errors.Enqueue(ErrorMessage("Multiline comment wasn't properly closed"));
                            }
                            break;
                        }
                    }
                    break;
                }
                default:
                    switch(current)
                    {
                        case ';': res.Add(Lexem.SemiColon.At(_line, _column)); break;
                        case '<': res.Add(Lexem.LessThan.At(_line, _column)); break;
                        case '>': res.Add(Lexem.GreaterThan.At(_line, _column)); break;
                        case '=': res.Add(Lexem.Equalz.At(_line, _column)); break;
                        case '(': res.Add(Lexem.OpenParant.At(_line, _column)); break;
                        case ')': res.Add(Lexem.CloseingParant.At(_line, _column)); break;
                        case '\n' or '\r':
                            break;
                        case ' ' or '\t': break;
                        case '+':
                        {
                            var lexem = IfMatchWhen(Lexem.Inc, Lexem.Add, '+');
                            if (lexem.Type == LexemType.Inc)
                                MoveNext();
                            res.Add(lexem.At(_line, _column));
                        } break;
                        case '-': 
                        {
                            var lexem = IfMatchWhen(Lexem.Dec, Lexem.Sub, '-');
                            if (lexem.Type == LexemType.Dec)
                                MoveNext();
                            res.Add(lexem.At(_line, _column));
                        } break;
                        case '/': res.Add(Lexem.Div.At(_line, _column)); break;
                        case '*': res.Add(Lexem.Mul.At(_line, _column)); break;
                        default:
                            Errors.Enqueue(ErrorMessage($"Unknown lexem '{current}'"));
                            break;
                    }
                    break;
            }
        }
        return res;
    }

    private Lexem IfMatchWhen(Lexem ifMatched, Lexem ifNotMatched, params char[] matches) => IsNextMatchTo(matches) ? ifMatched : ifNotMatched;

    private bool IsNextMatchTo(params char[] matches)
    {
        for (int i = _position + 1, j = 0; i < source.Length - 1 && j < matches.Length; i++, j++)
            if (matches.Contains(source[i])) return true;
        return false;
    }

    private string ConsumeWhile(Func<char, bool> predicate)
    {
        var sb = new StringBuilder();
        for (var current = source[_position]; predicate(current) && MoveNext(); current = source [_position])
            sb.Append(current);
        return sb.ToString();
    }

    private bool MoveNext() 
    {
        if (_position >= source.Length - 1) return false;
        _position++;
        return true;
    }

    private bool IsEnd() => !(_position < source.Length - 1);

    private string ErrorMessage(string msg) => $"[ERROR:{_line}:{_column}]:  {msg}";
}

