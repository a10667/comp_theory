﻿using System.Collections;

namespace Interp.Lib;

public class SymbolsTable: IEnumerable<SymbolsTable.Entry?>
{

    public event EventHandler<string> Comparison;

    public SymbolsTable():this(255)
    {
    }

    public SymbolsTable(int capacity)
    {
        _capacity = capacity;
        _data = new Entry?[_capacity];
    }

    public int Add(string value, SymbolDescription description)
    {
        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
            throw new ArgumentException("empty string");
        
        var hash = GetHash(value);
        var idx = hash % _capacity;

        while (_data[idx] is not null)
        {
            if (_data[idx]!.Value.Id == value) return idx;
            hash += 1;
            idx = hash % _capacity;
        }
        _data[idx] = new Entry(value, description);
        return idx;
    }

    public bool Contains(string value)
    {
        var hash = GetHash(value);
        var hashSaved = GetHash(value) + _capacity;
        var idx = hash % _capacity;
        if (_data[idx].HasValue && _data[idx]!.Value.Id == value)
        {
            OnComparison(value);
            return true;
        }

        do
        {
            hash += 1;
            idx = hash % _capacity;
            
            OnComparison(value);
            if (_data[idx].HasValue && _data[idx].Value.Id == value)
                return true;
        } while (_data[idx] is not null && hashSaved != hash);
        return false;
    }

    public void Clear()
    {
        _data = new Entry?[_capacity];
    }

    private void OnComparison(string id)
    {
        Comparison?.Invoke(this, id);
    }

    public IEnumerator<Entry?> GetEnumerator()
    {
        return new SymbolsTableEnumerator(_data);
    }

    public Entry? this[int i]
    {
        get => _data[i];
        set
        {
            _data[i] = value;
        }
    }

    public record struct Entry(string Id, SymbolDescription Description)
    {
        public override string ToString() => $"{Id}: {Description}";
    }


#region Private members
    private int _capacity = 255;

    private Entry?[] _data;

    private int GetHash(ReadOnlySpan<char> value)
    {
        if (value.Length < 2) return value[0];
        return value[0] + value[1];
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _data.GetEnumerator();
    }

    private class SymbolsTableEnumerator : IEnumerator<Entry?>
    {
        private int _position = -1;
        private Entry?[] _entreis;

        public SymbolsTableEnumerator(Entry?[] entreis)
        {
            _entreis = entreis;
        }

        public Entry? Current 
        {
            get 
            {
                if (_position == -1 || _position >= _entreis.Length)
                {
                    throw new ArgumentException();
                }
                return _entreis[_position];
            }
        }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_position < _entreis.Length - 1)
            {
                _position++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            _position = -1;
        }
    }

    #endregion
}

public abstract class SymbolDescription 
{

    public static Variable IntVar => new Variable(VariableType.Int);
    public static Variable FloatVar => new Variable(VariableType.Float);
    public static Variable UnknownVar => new Variable(VariableType.Unknown);

    public static Const IntConst => new Const(VariableType.Int);
    public static Const FloatConst => new Const(VariableType.Float);
    public static Const BoolConst => new Const(VariableType.Bool);

    public enum VariableType
    {
        Int, Float, Bool, Unknown
    }
    
    public class Array : SymbolDescription
    {
        public int Size { get; } 
        public VariableType Type { get; }

        private Array(){}
        public Array(int size, VariableType type)
        {
            Size = size;
            Type = type;
        }

        public override string ToString() => $"{Type}[{Size}]";
    }

    public class Variable : SymbolDescription
    {
        public VariableType Type { get; }

        private Variable(){}
        public Variable(VariableType type)
        {
            Type = type;
        }

        public Const ToConst() => new Const(Type);

        public override string ToString() => $"{Type}";
    }

    public class Const : SymbolDescription
    {
        public VariableType Type { get; }

        private Const(){}
        public Const(VariableType type)
        {
            Type = type;
        }

        public Variable ToVar() => new Variable(Type);

        public override string ToString() => $"Const {Type}";
    }
}
