using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Interp.Lib;

namespace Lab1.App.ViewModels;

public partial class MainWindowViewModel : ObservableObject
{
    public MainWindowViewModel()
    {
        _symbolTable = new SymbolsTable();
        _symbolTable.Comparison += (s, id) => { this.Comparisons++; };
    }

    private SymbolsTable _symbolTable;
    public List<string> RawIds { get; } = new();

    [ObservableProperty] private int _amountOfSearches = 0;
    [ObservableProperty] private int _comparisons = 0;
    [ObservableProperty] private int _amountOfComparisons = 0;
    [ObservableProperty] private float _averageAmountOfComparisons = 0;

    [ObservableProperty] private ObservableCollection<SymbolsTable.Entry> _dataFromFile = new();

    [ObservableProperty] private string _amountOfSearchesText = "Amount of searches: 0";

    [ObservableProperty] private string _thereWasNoSearches = "There was no searches";
    [ObservableProperty] private string _comparisonsText = "Comparisons: 0";
    [ObservableProperty] private string _amountOfComparisonsText = "Amount of comparisons: 0";
    [ObservableProperty] private string _averageAmountOfComparisonsText = "Average amount of comparisons: 0";

    [ObservableProperty] private string _filePath = "";

    [RelayCommand(CanExecute = nameof(CanSearch))]
    private void SearchId(string input)
    {
        bool wasFound = false;
        Comparisons = 0;
        if (_symbolTable.Contains(input))
        {
            ThereWasNoSearches = "Identifier was found";
            wasFound = true;
        }

        AmountOfComparisons += Comparisons;
        IncSearchCount(wasFound);
    }

    [RelayCommand]
    private void SearchAll()
    {
        if (DataFromFile.Count <= 0) return;

        bool wasFound = false;
        Comparisons = 0;
        foreach (var e in DataFromFile)
        {
            if (!_symbolTable.Contains(e.Id)) continue;
            ThereWasNoSearches = "Identifier was found";
            wasFound = true;
        }

        AmountOfComparisons += Comparisons;
        IncSearchCount(wasFound, DataFromFile.Count);
    }

    private bool CanSearch(string input)
    {
        return DataFromFile.Count > 0 && !string.IsNullOrEmpty(input) && !string.IsNullOrWhiteSpace(input);
    }

    [RelayCommand]
    private void Reset()
    {
        ThereWasNoSearches = "There was no search";
        AmountOfSearches = 0;
        Comparisons = 0;
        AmountOfComparisons = 0;
        AverageAmountOfComparisons = 0;
    }

    [RelayCommand]
    private void SelectFile()
    {
    }

    [RelayCommand(CanExecute = nameof(CanLoadFile))]
    private async Task LoadFileData(string input)
    {
        RawIds.AddRange(await File.ReadAllLinesAsync(input));

        foreach (var id in RawIds)
        {
            switch (id)
            {
                case var _ when int.TryParse(id, out _):
                    _symbolTable.Add(id, SymbolDescription.IntConst);
                    break;
                case var _ when float.TryParse(id, out _):
                    _symbolTable.Add(id, SymbolDescription.FloatConst);
                    break;
                case var _ when !string.IsNullOrEmpty(id) && !string.IsNullOrWhiteSpace(id):
                    _symbolTable.Add(id, SymbolDescription.UnknownVar);
                    break;
                default:
                    continue;
            }
        }

        DataFromFile.Clear();

        foreach (var symbol in _symbolTable)
        {
            if (symbol.HasValue)
                DataFromFile.Add(symbol.Value);
        }
    }

    private bool CanLoadFile(string input)
    {
        return !string.IsNullOrEmpty(input) && !string.IsNullOrWhiteSpace(input);
    }

    private void IncSearchCount(bool wasFound, int by = 1)
    {
        AmountOfSearches+=by;
        AverageAmountOfComparisons = AmountOfSearches / (float)AmountOfComparisons;
        if (!wasFound)
        {
            ThereWasNoSearches = "There was search";
        }
    }

    partial void OnComparisonsChanged(int value)
    {
         ComparisonsText= $"Comparisons: {value}";        
    }

    partial void OnAmountOfComparisonsChanged(int value)
    {
        AmountOfComparisonsText = $"Amount of comparisons: {value}";
    }

    partial void OnAverageAmountOfComparisonsChanged(float value)
    {
        AverageAmountOfComparisonsText = $"Average amount of comparisons: {value}";
    }

    partial void OnAmountOfSearchesChanged(int value)
    {
        AmountOfSearchesText = $"Amount of searches: {value}";
    }
}
