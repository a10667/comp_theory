using System.IO;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Platform.Storage;
using Lab1.App.ViewModels;

namespace Lab1.App;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void ExitButton_OnClick(object? sender, RoutedEventArgs e)
    {
       Close();
    }

    private async void SelectFileButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var files = await this.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions()
        {
            Title = "Select file",
            AllowMultiple = false,
            FileTypeFilter = new []{ FilePickerFileTypes.TextPlain }
        });

        var file = files.FirstOrDefault();
        if (file is null)
        {
            return;
        }

        ((MainWindowViewModel)DataContext).FilePath = file.Path.AbsolutePath;
        //((MainWindowViewModel)DataContext)!.RawIds.AddRange(await File.ReadAllLinesAsync(file.Path.AbsolutePath));
    }
}
