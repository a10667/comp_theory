using System;
using CommunityToolkit.Mvvm.ComponentModel;

namespace Lab2.App.ViewModels;

public partial class LexemTableRowViewModel : ObservableObject
{
    [ObservableProperty] private int _number;
    [ObservableProperty] private string _value = String.Empty;
    [ObservableProperty] private string _type = String.Empty;

    public LexemTableRowViewModel()
    {
    }

    public LexemTableRowViewModel(string type, int number, string value)
    {
        Type = type;
        Number = number;
        Value = value;
    }
}