using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Interp.Lib;

namespace Lab2.App.ViewModels;

public partial class MainWindowViewModel : ObservableObject
{
    public MainWindowViewModel()
    {
    }

    #region Private  members
    private SymbolsTable _symbolsTable = new();

    #region Source Tab

#if DEBUG
    [ObservableProperty] private string _filePath =
        "/home/danielf/Projects/cs/university/mer/comp_theory/assignments/examples/lab2/Lab2.txt";
#else
    [ObservableProperty] private string _filePath = String.Empty;
#endif
    [ObservableProperty] private string _sourceCode = String.Empty;

    #endregion

    #region Lexem table Tab

    [ObservableProperty] private ObservableCollection<LexemTableRowViewModel> _lexemTable = new();

    #endregion

    #region SymbolTable Tab
    
    [ObservableProperty] private ObservableCollection<SymbolsTable.Entry> _symbolTableView = new();
    #endregion
    
    #region Errors Tab
    [ObservableProperty] private ObservableCollection<string> _lexerErrors = new();
    #endregion

    [RelayCommand(CanExecute = nameof(CanLoadFile))]
    private async Task LoadFile(string input)
    {
        SourceCode = await File.ReadAllTextAsync(input);
        var lexer = new Lexer(SourceCode, _symbolsTable);
        var lexems = lexer.Lex();
        foreach (var lexem in lexems)
        {
            LexemTable.Add(new LexemTableRowViewModel()
            {
                Type =  lexem.Type.ToString(),
                Number = lexem.Pointer,
                Value = lexem.Value
            });
        }

        var sa = new SyntaxAnalyzer(lexems, _symbolsTable);
        sa.Analyze();

        foreach (var symbol in _symbolsTable)
        {
            if (symbol.HasValue)
            {
                SymbolTableView.Add(symbol.Value);
            }
        }

        foreach (var er in lexer.Errors)
        {
            LexerErrors.Add(er);
        }
    }

    private bool CanLoadFile(string input) => !string.IsNullOrWhiteSpace(input) && !string.IsNullOrEmpty(input);
    #endregion
}