using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Platform.Storage;
using Lab2.App.ViewModels;

namespace Lab2.App;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void ExitButton_OnClick(object? sender, RoutedEventArgs e)
    {
        Close();
    }

    private async void SelectFileButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var files = await this.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions()
        {
            Title = "Select file",
            AllowMultiple = false,
            FileTypeFilter = new []{ FilePickerFileTypes.TextPlain }
        });

        var file = files.FirstOrDefault();
        if (file is null)
        {
            return;
        }

        ((MainWindowViewModel)DataContext).FilePath = file.Path.AbsolutePath;
    }
}