#!/bin/bash

# cleanup
#

rm -rf ./artifacts/
mkdir ./artifacts

rm -rf ./src/Interp.Lib/bin/
rm -rf ./src/Lab1.App/bin/
rm -rf ./src/Lab2.App/bin/
rm -rf ./src/Lab3.App/bin/

rm -rf ./src/Interp.Lib/obj/
rm -rf ./src/Lab1.App/obj/
rm -rf ./src/Lab2.App/obj/
rm -rf ./src/Lab3.App/obj/

# build

movetoarts () {
    cd ./artifacts/
    mkdir "Lab$1.App"
    mv "../src/Lab$1.App/bin/Release/net8.0/linux-x64/" "./Lab$1.App"
    cd ..
}

if [[ "$1" == "release" ]]; then
    dotnet restore && dotnet build -c Release --self-contained true
    movetoarts 1
    movetoarts 2
    movetoarts 3
elif [[ "$1" == "publish" ]]; then
    if [[ "$2" == "win" ]]; then
        dotnet publish -r win-x64 -p:PublishSingleFile=true --self-contained false
    else
        dotnet publish -r linux-x64 -p:PublishSingleFile=true --self-contained false
    fi
    movetoarts 1
    movetoarts 2
    movetoarts 3
else 
    dotnet restore && dotnet build
fi

