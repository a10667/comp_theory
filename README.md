# labs

## lab1 

```bash
./artifacts/Lab1.App/linux-x64/publish/Lab1.App
```
Во всех вариантах требуется разработать программу, реализующую комбинированный способ
организации таблицы идентификаторов. Для организации таблицы используется простейшая хэш-
функция, указанная в варианте задания, а при возникновении коллизий используется дополнительный
метод размещения идентификаторов в памяти. Если в качестве этого метода используется дерево или
список, то они должны быть связаны с элементом главной хэш-таблицы.
11
В каждом варианте требуется, чтобы программа сообщала среднее число коллизий и среднее
количество сравнений, выполненных для поиска идентификатора

| hash func | способ разрешения коллизии |
| -------------- | --------------- |
| сумма кодов 1ой и 2ой букв | Простое рехеширование |


## lab2

```bash
./artifacts/Lab2.App/linux-x64/publish/Lab2.App
```
Для выполнения лабораторной работы требуется написать программу, которая
выполняет лексический анализ входного текста в соответствии с заданием и порождает
таблицу лексем с указанием их типов и значений. Текст на входном языке задается в виде
символьного (текстового) файла. Программа должна выдавать сообщения о наличие во
входном тексте ошибок, которые могут быть обнаружены на этапе лексического анализа.

Длину идентификаторов и строковых констант считать ограниченной 32 символами.
Программа должна допускать наличие комментариев неограниченной длины во входном
файле. Форму организации комментариев предлагается выбрать самостоятельно.
1. Входной язык содержит арифметические выражения, разделенные символом
;(точка с запятой). Арифметические выражения состоят из идентификаторов, десятичных
чисел с плавающей точкой (в обычной и логарифмической форме), знака присваивания (:=),
знаков операций +, -, *, / и круглых скобок.
2. Входной язык содержит логические выражения, разделенные символом ;(точка с
запятой). Логические выражения состоят из идентификаторов, констант true и false, знака
присваивания (:=), знаков операций or, xor, and, not и круглых скобок.
3. Входной язык содержит операторы условия типа if … then … else и if … then,
разделенные символом ;(точка с запятой). Операторы условия содержат идентификаторы,
знаки сравнения <, >, =, десятичные числа с плавающей точкой (в обычной и
логарифмической форме), знак присваивания (:=).
4. Входной язык содержит операторы цикла типа for (…; …; …) do, разделенные
символом ;(точка с запятой). Операторы цикла содержат идентификаторы, знаки сравнения
<, >, =, десятичные числа с плавающей точкой (в обычной и логарифмической форме), знак
присваивания (:=).

# build

## debug build

```sh
./build.sh
```

## release build

```sh
./build.sh release
```

# publishing

## linux
```sh
./build.sh publish
```

## window
```sh
./build.sh publish
```

# dependencies

.NET core >= 8.0.101 

Avalonia  >= 11.0.10
